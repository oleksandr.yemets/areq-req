import asyncio
import logging
import re
import sys
import json
import time
from typing import IO
import urllib.error
import urllib.parse

import aiohttp
from aiohttp import ClientSession

logging.basicConfig(
    format="%(asctime)s %(levelname)s:%(name)s: %(message)s",
    level=logging.DEBUG,
    datefmt="%H:%M:%S",
    stream=sys.stderr,
)
logger = logging.getLogger("areq")
logging.getLogger("chardet.charsetprober").disabled = True


async def fetch_user(session: ClientSession) -> None:
    """Find user attrs in response results."""
    url = 'https://randomuser.me/api/'
    try:
        resp = await session.request(method="GET", url=url)
        resp.raise_for_status()
        logger.info("Got response [%s]", resp.status)
        # simulating computation delays
        await asyncio.sleep(1)
        response = await resp.json()
    except (
        aiohttp.ClientError,
        aiohttp.http_exceptions.HttpProcessingError,
    ) as e:
        logger.error(
            "aiohttp exception for %s [%s]: %s",
            url,
            getattr(e, "status", None),
            getattr(e, "message", None),
        )
    except Exception as e:
        logger.exception(
            "Non-aiohttp exception occured:  %s", getattr(e, "__dict__", {})
        )
    else:
        for item in response.get('results'):      
            name = item.get('name')
            logger.info(f"Found {name.get('title')} {name.get('first')} {name.get('last')}")


async def gather_tasks() -> None:
    """Gather few concurrent url requests."""
    async with ClientSession() as session:
        tasks = []
        for _ in list(range(5)):
            tasks.append(
                fetch_user(session=session)
            )
        await asyncio.gather(*tasks)

if __name__ == "__main__":
    import sys

    assert sys.version_info >= (3, 7), "Script requires Python 3.7+."
    
    start = time.perf_counter()
    asyncio.run(gather_tasks())
    elapsed = time.perf_counter() - start
    logger.info(f"Program completed in {elapsed:0.5f} seconds.")
