import requests
import logging
import re
import sys
import json
import time
from typing import IO

logging.basicConfig(
    format="%(asctime)s %(levelname)s:%(name)s: %(message)s",
    level=logging.DEBUG,
    datefmt="%H:%M:%S",
    stream=sys.stderr,
)
logger = logging.getLogger("areq")
logging.getLogger("chardet.charsetprober").disabled = True


def fetch_user() -> None:
    """Find user attrs in response results."""
    url = 'https://randomuser.me/api/'
    try:
        resp = requests.get(url=url)
        logger.info("Got response [%s]", resp.status_code)
        # simulating computation delays
        time.sleep(1)
        response = resp.json()
    except Exception as e:
        logger.exception(
            "Non-aiohttp exception occured:  %s", getattr(e, "__dict__", {})
        )
    else:
        for item in response.get('results'):      
            name = item.get('name')
            logger.info(f"Found {name.get('title')} {name.get('first')} {name.get('last')}")


def gather_tasks() -> None:
    """Gather few concurrent url requests."""
    for _ in list(range(5)):
        fetch_user()

if __name__ == "__main__":
    import sys

    assert sys.version_info >= (3, 7), "Script requires Python 3.7+."
    
    start = time.perf_counter()
    gather_tasks()
    elapsed = time.perf_counter() - start
    logger.info(f"Program completed in {elapsed:0.5f} seconds.")
